import unittest

from game.hand import Hand
from game.game import Game
from models.card import Card
from models.user import User


class TestHand(unittest.TestCase):
    def test_is_blackjack(self):
        try:
            user = User("Mock_user", "asdf")
        except:
            user = User.get_user_by_email("Mock_user")
        ace = Card("D", 1)
        king = Card("D", 13)
        jack = Card("D", 11)
        two = Card("D", 2)
        blackjack = Hand(user, ace, king)
        blackjack2 = Hand(user, king, ace)
        not_blackjack = Hand(user, jack, king)
        blackjack3 = Hand(user, two, two, two, two ,two)
        self.assertTrue(blackjack.is_blackjack(), "blackjack is not blackjack")
        self.assertTrue(blackjack2.is_blackjack(), "blackjack2 is not blackjack")
        self.assertFalse(not_blackjack.is_blackjack(), "not_blackjack is blackjack")
        self.assertTrue(blackjack3.is_blackjack(), "blackjack3 is not blackjack")

    def test_value_of_hand(self):
        try:
            user = User("Mock_user", "asdf")
        except:
            user = User.get_user_by_email("Mock_user")
        ace = Card("D", 1)
        king = Card("D", 13)
        jack = Card("D", 11)
        two = Card("D", 2)
        blackjack = Hand(user, ace, king)
        blackjack2 = Hand(user, king, ace)
        not_blackjack = Hand(user, jack, king)
        blackjack3 = Hand(user, two, two, two, two, two)
        hand_14 = Hand(user, ace, ace, jack, two)
        hand_12 = Hand(user, ace, ace)
        self.assertEqual(blackjack.value_of_hand(), 21)
        self.assertEqual(blackjack2.value_of_hand(), 21)
        self.assertEqual(blackjack3.value_of_hand(), 21)
        self.assertEqual(not_blackjack.value_of_hand(), 20)
        self.assertEqual(hand_14.value_of_hand(), 14)
        self.assertEqual(hand_12.value_of_hand(), 12)


if __name__ == '__main__':
    unittest.main()

from models.user import User
from database import connection
import database
from game.game import Game


def stats_on_how_many_games_played():
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(database.GET_TOTAL_NUMBER_OF_GAMES)
            return cursor.fetchone()[0]

def stats_on_player(email):
    """Return (win, tie, loss) tuple."""
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(database.GET_USER_STATS, (email,))
            return cursor.fetchone()
